
import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { FollowingArtist } from '../../models/Artist/followingArtist';
import { Observable } from 'rxjs/Observable';
import 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
    userFollowingArtist: FollowingArtist[];
    userToken: string;

    constructor(private http: Http) { }

    getFollowingArtists() : Observable<FollowingArtist[]>{
        return this.http.get("following.json")
            .map((response: Response) =><FollowingArtist[]>response.json())
            .do((data) => {
                this.userFollowingArtist = data;
            })
            .catch(this.handleError);
    }

    setUserToken(userToken: string): void{
        this.userToken = userToken;
    }

    getUserToken(): string{
        return this.userToken;
    }

    private handleError(errorResponse: Response){
        console.error(errorResponse);
        return Observable.throw(errorResponse.json().error || "Server Error");
    }
}