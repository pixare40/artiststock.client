import { Injectable } from '@angular/core';

@Injectable()
export class CommunicationsService {

    private baseUrl = "http://localhost:4000";
    private requestUrls: {};

    constructor() { 
        this.requestUrls = {};
        this.requestUrls["log-in"] = "/api/v1/log-in";
        this.requestUrls["register"] = "/api/v1/register"
    }

    getUrl(requestType: string): string{
        if(this.requestUrls[requestType] == null){
            return null;
        }

        return this.baseUrl + this.requestUrls[requestType];
    }

    sendRequest() : any{
        return null;
    }
}