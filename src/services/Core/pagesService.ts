
import { Injectable } from '@angular/core';
import { LoginPage, HomePage, BookmarksPage, ArtistPage, ExchangePage, ProfilePage, RegistrationPage, SettingsPage, TrackingPage } from "../../pages/pages";

@Injectable()
export class PagesService {

    pages: Array<{title: string, component: any, icon: string}>;

    constructor() {

        this.pages = [
                { title: 'Stream', component: HomePage, icon: 'list' },
                { title: 'Exchange', component: ExchangePage, icon: 'analytics' },
                { title: 'Tracking', component: TrackingPage, icon: 'bonfire' },
                { title: 'Bookmarks', component: BookmarksPage, icon: 'bookmark' },
                { title: 'Profile', component: ProfilePage, icon: 'contact' },
                { title: 'Settings', component: SettingsPage, icon: 'hammer'  }
        ];
     }

     getPages() : Array<{title: string, component: any}>{
         return this.pages;
     }
}