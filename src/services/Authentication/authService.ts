import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { CommunicationsService } from "../Core/communicationsService"
import { RegistrationModel, LoginModel, IAutheticationModel } from "../../models/models";

@Injectable()
export class AuthService{
    private authToken;

    constructor(private http:Http, private communicationsService: CommunicationsService){

    }

    login(accountDetails: {}) : Observable<IAutheticationModel>{
        return this.http.post(this.communicationsService.getUrl("log-in"), accountDetails)
        .map((response:Response)=><IAutheticationModel>response.json())
        .do(data => {
            this.authToken = data.jwt;
        })
        .catch(this.handleError);
    }

    register(registrationModel: RegistrationModel) : Observable<Response>{
        return this.http.post(this.communicationsService.getUrl("register"), registrationModel);
    }

    private handleError(errorResponse: Response){
        console.error(errorResponse);
        return Observable.throw(errorResponse.json().error || "Server Error");
    }
}
