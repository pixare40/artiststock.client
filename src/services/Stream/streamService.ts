
import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class StreamService {

    news: any;

    constructor(private http: Http) { 
    }

    getNews() : Observable<any>{
        return this.http.get("news.json")
                        .map((response:Response)=><any>response.json())
                        .do(data => {
                            this.news = data;
                        })
                        .catch(this.handleError);
    }

    handleError(errorResponse: Response): any{
        console.log("An error occured fetching news");
         return Observable.throw(errorResponse || "Server Error");
    }
}