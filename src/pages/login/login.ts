import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { RegistrationPage } from "../register/register";
import { AuthService } from "../../services/services";
import { RegistrationModel, LoginModel, IAutheticationModel } from "../../models/models";

@Component({
    templateUrl: "login.html"
})
export class LoginPage{

    loginModel: LoginModel;
    authenticationModel: IAutheticationModel;
    errorMessage: string;
    successMessage: string;

    constructor(private navController: NavController
    ,private viewController: ViewController
    ,private authService: AuthService){
        this.loginModel = new LoginModel();
    }

    dismiss(){
        this.viewController.dismiss();
    }

    login(){
        if(this.loginModel.username && this.loginModel.password){
            let account = {};
            account["email"] = this.loginModel.username;
            account["password"] = this.loginModel.password;
            this.authService.login(account).subscribe(data => {
                if (data.jwt != null){
                    this.successMessage = "Success";
                }
            })
        }
        else{
            this.errorMessage = "Enter login details"
        }
    }

    goToRegistration(){
        this.navController.push(RegistrationPage);
    }
}