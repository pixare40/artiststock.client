
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/User/userService';
import { FollowingArtist } from '../../models/Artist/followingArtist';

@Component({
    templateUrl: 'tracking.html'
})
export class TrackingPage implements OnInit {

    followingArtist: FollowingArtist[];
    currentCategory: string;

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.currentCategory = "following";
        this.userService.getFollowingArtists().subscribe((data) =>{
            this.followingArtist = data;
            console.log("fetched artists");
        })
     }
}