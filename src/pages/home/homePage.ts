import { Component, OnInit } from "@angular/core";
import { NavController, ModalController } from "ionic-angular";
import { LoginPage } from "../pages";
import { StreamService } from "../../services/Stream/streamService";
import {DomSanitizer} from '@angular/platform-browser';
import { NewsModel } from "../../models/stream/newsItem";

@Component({
    templateUrl: "home.html",
})
export class HomePage implements OnInit {

    public news: Array<NewsModel>;

    constructor(private navController: NavController, 
                private modalController: ModalController, 
                private streamService: StreamService,
                private sanitizer: DomSanitizer){
    }

    ngOnInit(): void {
        this.streamService.getNews().subscribe(data=>{
            if(data!=null){
                console.log("Succesfully fetched news");
                for (let item of data){
                    let untrustedUrl = item.news_item_image;
                    let trustedUrl = this.sanitizer.bypassSecurityTrustStyle('url(' + untrustedUrl + ')');
                    item.news_item_image = trustedUrl;
                }

                this.news = data;
            }
            else{
                console.log("News not fetched");
            }
        })
    }

    login(){
        let loginModal = this.modalController.create(LoginPage);
        loginModal.present();
    }
}