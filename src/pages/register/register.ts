import { Component } from "@angular/core";
import { Response } from "@angular/http";
import { NavController } from "ionic-angular";
import { RegistrationModel } from "../../models/models";
import { AuthService } from "../../services/services";

@Component({
    templateUrl: "register.html",
})
export class RegistrationPage{
    registerModel: RegistrationModel;
    successString: string;
    errorString: string;

    constructor(private navController: NavController, private authService: AuthService){
        this.registerModel = new RegistrationModel("","","");
    }

    register(){
        this.authService.register(this.registerModel).subscribe((response:Response)=>{
            this.successString = "Success";
        });
    }
}