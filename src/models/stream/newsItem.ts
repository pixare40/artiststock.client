
export class NewsModel{
    _id: string;
    tags: Array<string>;
    news_item_image: any;
    title: string;
    summary: string;
    news_story: string;
    date_fetched: string;
    date_created: string;
    url_to_story: string;
}