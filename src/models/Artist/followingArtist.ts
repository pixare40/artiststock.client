
export class FollowingArtist{
    _id: string;
    artist_display_name: string;
    artist_c_name: string;
    artist_thumb: string;
}