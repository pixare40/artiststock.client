import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { CommunicationsService, PagesService } from "../services/services"
import { HomePage, LoginPage } from "../pages/pages";

@Component({
  templateUrl: 'app.html',
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public modalController: ModalController, private pagesService: PagesService) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = this.pagesService.getPages();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  showLogin(){
    let loginModal = this.modalController.create(LoginPage)
    loginModal.present();
  }
}
