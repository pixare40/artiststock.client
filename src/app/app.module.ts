import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { LoginPage, HomePage, BookmarksPage, ArtistPage, ExchangePage, ProfilePage, RegistrationPage, SettingsPage, TrackingPage } from "../pages/pages";
import { Http } from "@angular/http";
import { CommunicationsService, AuthService, PagesService, StreamService, UserService, ArtistService } from "../services/services";


@NgModule({
  declarations: [
      MyApp,
      HomePage,
      LoginPage,
      RegistrationPage,
      BookmarksPage,
      ArtistPage,
      ExchangePage,
      ProfilePage,
      SettingsPage, 
      TrackingPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
      MyApp,
      HomePage,
      LoginPage,
      RegistrationPage,
      BookmarksPage,
      ArtistPage,
      ExchangePage,
      ProfilePage,
      SettingsPage, 
      TrackingPage
  ],
  providers: [CommunicationsService, AuthService, PagesService, StreamService, ArtistService, UserService]
})
export class AppModule {}
